# LazyElements

LazyElements is a gem that can lazy load html template through jQuery and web api.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lazy_elements'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lazy_elements

Run:

    $ rails g lazy_elements:install


Add the following to application.js

```
//=require lazy_elements
```


## Basic Usage

In our view file write:


```
  = lazy_element(:your_partial_template_name)
```

And add template to:

```
  app/view/api/lazy_elements/elements/your_partial_template_name

```

### Use Attributes

If you want to pass a value for template, you can use hash in the name of attr


app/views/posts/show.html.haml

```
  = lazy_element(:lazy_template, {attr: {id: 1, name: 'hoge'}})
```

app/view/api/lazy_elements/elements/_lazy_template.html.haml
```
.test_template
 = attr[:id]
 = attr[:name]
```




## Configuration

You are available some options

- template_path
- class_name

config/initializers/lazy_elements.rb

```ruby
LazyElements.configure do |config|
  config.template_path = 'your template path'
  config.class_name    = 'your class name'
end
```

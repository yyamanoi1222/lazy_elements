class Api::LazyElements::ElementsController < ApplicationController
  def show
    name = params[:id]
    attr = params[:attr]

    file_path = "#{LazyElements.config.template_path}#{name}"
    template_str = render_to_string(partial: file_path, locals: {attr: attr})
    render json: {template: template_str}
  end
end

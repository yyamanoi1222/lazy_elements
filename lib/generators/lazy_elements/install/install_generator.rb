module LazyElements
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      desc "This generator write a route for lazy_elements api"

      def write_lazy_elements_route
        lazy_elements_route = "namespace :api do\n"
        lazy_elements_route << "     namespace :lazy_elements do\n"
        lazy_elements_route << "      resource :elements"
        lazy_elements_route << ", only: :show\n"
        lazy_elements_route << "    end\n"
        lazy_elements_route << "  end"
        route lazy_elements_route
      end
    end
  end
end

require "lazy_elements/version"
require "lazy_elements/config"
require "lazy_elements/engine"
require "lazy_elements/view_helpers"

module LazyElements
end

ActionView::Base.send :include, LazyElements::ViewHelpers

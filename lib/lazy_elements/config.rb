require 'active_support/configurable'

module LazyElements
  def self.default_controller_path
    '/api/lazy_elements/elements'
  end

  def self.default_template_path
    'api/lazy_elements/elements/'
  end

  def self.default_class_name
    'lazy_element'
  end

  def self.configure(&block)
    yield @config ||= LazyElements::Configuration.new
  end

  def self.config
    @config
  end

  class Configuration
    include ActiveSupport::Configurable
    config_accessor :controller_path
    config_accessor :template_path
    config_accessor :class_name
  end

  configure do |config|
    config.controller_path = LazyElements.default_controller_path
    config.template_path   = LazyElements.default_template_path
    config.class_name      = LazyElements.default_class_name
  end
end


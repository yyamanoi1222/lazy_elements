module LazyElements
  module ViewHelpers
    def lazy_element(name, options = {})
      check_template_file!(name)
      render_lazy_blank_element(name, options[:attr])
    end

    def render_lazy_blank_element(name, attr)
      default_option = {'data-id' => name, 'class' => LazyElements.config.class_name}

      option = default_option

      if attr.is_a?(Hash)
        option['data-attr'] = attr.to_json
      end


      content_tag(:div, '', option)
    end

    private

    def check_template_file!(name)
      path = LazyElements.config.template_path

      unless lookup_context.exists?(name.to_s, path, true)
        raise ActionView::ActionViewError.new("Template missing #{name.to_s} in #{path}")
      end
    end
  end
end
